package com.jakubkoci.asymcrypt;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class App {

	static void exportKeys(Key key, String publicFilename, String privateFilename) {
		App.saveToFile(key.getPublicKey(), publicFilename);
		App.saveToFile(key.getPrivateKey(), privateFilename);
	}

	static void saveToFile(Object object, String fileName) {
		ObjectOutputStream output = null;
		try {
			output = new ObjectOutputStream(new FileOutputStream(fileName));
			output.writeObject(object);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("File was not found!", e);
		} catch (IOException e) {
			throw new IllegalArgumentException("Error writing to file!", e);
		} finally {
			try {
				if (output != null) {
					output.close();	
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	static String encryptMessage(String message,
			String publicKeyFilename) {
		try {
			PublicKey publicKey = (PublicKey) App.loadFromFile(publicKeyFilename);
			RSA rsa = new RSA();
			return rsa.encryptText(message, publicKey);
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("Wrong file format! Public key is expected.", e);
		}
	}

	static String decryptMessage(String message, String privateKeyFilename) {
		try {
			PrivateKey privateKey = (PrivateKey) App.loadFromFile(privateKeyFilename);
			RSA rsa = new RSA();
			return rsa.decryptText(message, privateKey);
		} catch (ClassCastException e) {
			throw new IllegalArgumentException("Wrong file format! Private key is expected.", e);
		}
	}

	static Object loadFromFile(String publicKeyFilename) {
		ObjectInputStream input = null;
		Object result = null;
		try {
			input = new ObjectInputStream(new FileInputStream(publicKeyFilename));
			result = input.readObject();
		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException("Wrong file format!", e);
		} catch (FileNotFoundException e) {
			throw new IllegalArgumentException("File was not found!", e);
		} catch (IOException e) {
			throw new IllegalArgumentException("Error reading from file!", e);
		} finally {
			try {
				if (input != null) {
					input.close();	
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

}
