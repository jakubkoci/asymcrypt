package com.jakubkoci.asymcrypt;

import java.io.Serializable;
import java.math.BigInteger;

public class PrivateKey implements Serializable {

	private BigInteger numberD;
	private BigInteger numberN;

	public PrivateKey(BigInteger numberD, BigInteger numberN) {
		this.numberD = numberD;
		this.numberN = numberN;
	}

	public BigInteger getNumberD() {
		return numberD;
	}

	public BigInteger getNumberN() {
		return numberN;
	}

	@Override
	public String toString() {
		return "PrivateKey [numberD=" + numberD + ", numberN=" + numberN + "]";
	}
}

