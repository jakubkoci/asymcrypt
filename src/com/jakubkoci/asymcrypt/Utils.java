package com.jakubkoci.asymcrypt;

import java.math.BigInteger;

public class Utils {
	
	private static final int CERTAINTY = 10;

	public static boolean isPrimeNumber(long number) {
		if (number == 1) {
			return true;
		}
		for (long factor = 2; factor < number; factor++) {
			if (number % factor == 0) {
				return false;
			}
		}
		return true;
	}

	public static boolean areRelativelyPrime(BigInteger a, BigInteger b) {
		// TODO This is hack and may would be replaced with relatively prime numbers calculation algorithm
		return a.isProbablePrime(CERTAINTY);
	}

	public static BigInteger roundedLogBaseTwo(BigInteger value) {
		double logN = Math.log(value.longValue()) / Math.log(2);
		BigInteger roundedLogN = BigInteger.valueOf(Math.round(logN));
		return roundedLogN;
	}

	public static BigInteger multiplyAndModuloRemainder(BigInteger a, BigInteger b, BigInteger c) {
		BigInteger[] result = a.multiply(b).divideAndRemainder(c);
		return result[1];
	}
}
