package com.jakubkoci.asymcrypt;


public class ConsoleApp {

	public static void main(String[] args) {
		if (args != null && args.length > 0) {
			String cmd = args[0];
			if ("gen".equals(cmd)) {
				String publicKeyFilename = args[1];
				String privateKeyFilename = args[2];
				App.exportKeys(Key.generateRandom(), publicKeyFilename, privateKeyFilename);				
			} else if ("encrypt".equals(cmd)) {
				String publicKeyFilename = args[1];
				String message = args[2];
				String encrypted = App.encryptMessage(message, publicKeyFilename);
				System.out.println(encrypted);
			} else if ("decrypt".equals(cmd)) {
				String privateKeyFilename = args[1];
				String message = args[2];
				String decrypted = App.decryptMessage(message, privateKeyFilename);
				System.out.println(decrypted);
			} else if ("help".equals(cmd)) {
				System.out.println("gen <publicKeyFilename> <privateKeyFilename>");
				System.out.println("encrypt <publicKeyFilename> <originalMessage>");
				System.out.println("decrypt <privateKeyFilename> <encryptedMessage>");
			} else {
				throw new IllegalArgumentException("Wrong parameter! Fill in gen, encrypt or decrypt.");
			}
		} else {
			throw new IllegalArgumentException("Missing some parameter! Fill in gen, encrypt or decrypt.");
		}
	}
	
}
