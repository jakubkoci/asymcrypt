package com.jakubkoci.asymcrypt;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class NumberDIntervalProcessor {

	private static final int THREADS_COUNT = 6;
	Boolean lock = false;
	BigInteger result;
	List<Thread> threads;
	private BigInteger eulerFunction;
	private BigInteger e;

	public NumberDIntervalProcessor(BigInteger e, BigInteger eulerFunction, BigInteger begin, BigInteger end) {
		super();
		this.eulerFunction = eulerFunction;
		this.e = e;
		createIntervals(begin, end);
	}

	private void createIntervals(BigInteger begin, BigInteger end) {
		BigDecimal intervalSize = BigDecimal.valueOf(end.subtract(begin).longValue());
		BigDecimal count = BigDecimal.valueOf(THREADS_COUNT);
		BigInteger subintervalSize = intervalSize.divide(count, RoundingMode.UP).toBigInteger();

//		System.out.println("Subinterval " + subintervalSize);

		threads = new ArrayList<Thread>();
		for (BigInteger i = BigInteger.ZERO; i.compareTo(count.toBigInteger()) < 0; i = i.add(BigInteger.ONE)) {
			BigInteger subintervalBegin = i.multiply(subintervalSize).add(begin);
			BigInteger subintervalEnd = i.multiply(subintervalSize).add(subintervalSize).add(begin);
			if (subintervalEnd.compareTo(end) > 0) {
				subintervalEnd = end;
			}
			String threadName = "T " + i.add(BigInteger.ONE);
			Interval interval = new Interval(subintervalBegin, subintervalEnd);
//			System.out.println(interval);
			Thread t = new Thread(interval, threadName);
			threads.add(t);
		}
	}

	public void startThreads() {
		for (Thread t : threads) {
			t.start();
		}
		for (Thread t : threads) {
			try {
				t.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private class Interval implements Runnable {

		private BigInteger begin;
		private BigInteger end;

		public Interval(BigInteger begin, BigInteger end) {
			super();
			this.begin = begin;
			this.end = end;
		}

		@Override
		public String toString() {
			return "Interval [begin=" + begin + ", end=" + end + "]";
		}

		@Override
		public void run() {
			findNumberDInInterval(e);
		}

		private BigInteger findNumberDInInterval(BigInteger e) {
			for (BigInteger d = begin; d.compareTo(end) < 0; d = d.add(BigInteger.ONE)) {
//				System.out.println(Thread.currentThread().getName() + " " + d);
				
				BigInteger remainder = Utils.multiplyAndModuloRemainder(d, e, eulerFunction); 
				if (BigInteger.ONE.equals(remainder) && !d.equals(e)) {
					synchronized (lock) {
						if (Boolean.FALSE.equals(lock)) {
							result = d;
							lock = Boolean.TRUE;
//							System.out.println(Thread.currentThread().getName() + " Number was found!");
						} else {
							break;
						}
					}
				}
				if (Boolean.TRUE.equals(lock)) {
					break;
				}
			}
			return BigInteger.ZERO;
		}
	}
}
