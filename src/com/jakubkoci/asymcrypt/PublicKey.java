package com.jakubkoci.asymcrypt;

import java.io.Serializable;
import java.math.BigInteger;

public class PublicKey implements Serializable {

	private BigInteger numberE;
	private BigInteger numberN;
	
	public PublicKey(BigInteger numberE, BigInteger numberN) {
		this.numberE = numberE;
		this.numberN = numberN;
	}

	public BigInteger getNumberE() {
		return numberE;
	}

	public BigInteger getNumberN() {
		return numberN;
	}

	@Override
	public String toString() {
		return "PublicKey [numberE=" + numberE + ", numberN=" + numberN + "]";
	}
}
