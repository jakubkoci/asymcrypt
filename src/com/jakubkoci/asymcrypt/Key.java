package com.jakubkoci.asymcrypt;

import java.math.BigInteger;
import java.util.Random;

public class Key {

	private static final long MIN_PRIME_VALUE = 100;
	private static final int DEFAULT_PRIME_NUMBER_BIT_LENGTH = 8;
	private static final BigInteger NUMBER_D_START = BigInteger.valueOf(100000000);
	private static final BigInteger NUMBER_D_LIMIT = BigInteger.valueOf(Integer.MAX_VALUE).subtract(NUMBER_D_START);
	
	private long p;
	private long q;
	private BigInteger numberN;
	private BigInteger eulerFunction;
	private BigInteger numberE;
	private BigInteger numberD;
	
	public static Key generateRandom() {
		return generateRandom(DEFAULT_PRIME_NUMBER_BIT_LENGTH);
	}
	
	public static Key generateRandom(int bitLength) {
		Random random = new Random();
		long p = BigInteger.probablePrime(bitLength, random).longValue();
		long q = BigInteger.probablePrime(bitLength, random).longValue();
		return new Key(p, q);
	}
	
	public static Key generateFromNumbers(long p, long q) {
		return new Key(p, q);
	}
	
	private Key(long p, long q) {
		if (areValidPrimeNumbers(p, q)) {
//			System.out.println("Prime numbers are valid");
			this.p = p;		
			this.q = q;
			
			this.numberN = calculateN();
//			System.out.println("Number N = " + numberN);
			
			this.eulerFunction = calculateEulerFunction();
//			System.out.println("Euler function was generated");
			
			this.numberE = chooseE();
//			System.out.println("Number E = " + numberE);
			if (!BigInteger.ZERO.equals(this.numberE)) {
				this.numberD = findD(numberE);
//				System.out.println("Number D was found!");	
			}
			
			if (this.numberD == null) {
				throw new IllegalArgumentException("Key generating failed! Try it again!");
			}
		} else {
			throw new IllegalArgumentException("Given parameters shloud be prime number higher than" + MIN_PRIME_VALUE + "!");
		}
	}
	
	private boolean areValidPrimeNumbers(long p, long q) {
		if (p > MIN_PRIME_VALUE && q > MIN_PRIME_VALUE) {
			return Utils.isPrimeNumber(p) && Utils.isPrimeNumber(q);	
		}
		return false;
	}

	private BigInteger calculateN() {
		return BigInteger.valueOf(p * q);
	}

	private BigInteger calculateEulerFunction() {
		return BigInteger.valueOf((p - 1) * (q - 1));
	}

	private BigInteger chooseE() {
		BigInteger intervalBegin = BigInteger.valueOf(Math.max(p + 1, q + 1));
		BigInteger intervalEnd = eulerFunction;
		
		for (BigInteger e = intervalBegin; e.compareTo(intervalEnd) < 0; e = e.add(BigInteger.ONE)) {
			if (Utils.areRelativelyPrime(e, eulerFunction)) {
				return e;
			}
		}
		return BigInteger.ZERO;
	}

	private BigInteger findD(BigInteger e) {
		BigInteger logN = Utils.roundedLogBaseTwo(numberN);
		
		long startTime = System.currentTimeMillis();
		NumberDIntervalProcessor intervalProcessor = new NumberDIntervalProcessor(e, eulerFunction, NUMBER_D_START, NUMBER_D_LIMIT);
		intervalProcessor.startThreads();
	
//		System.out.println(intervalProcessor.result);
		long finishTime = System.currentTimeMillis();
		
//		System.out.println("Time elapsed: " + (finishTime - startTime));
		
		numberD = intervalProcessor.result;
		return numberD;
	}

	public PublicKey getPublicKey() {
		return new PublicKey(numberE, numberN);
	}

	public PrivateKey getPrivateKey() {
		return new PrivateKey(numberD, numberN);
	}
	
	@Override
	public String toString() {
		return "Key [\n" 
				+ "p=" + p + ", q=" + q + "\n" 
				+ "numberN=" + numberN + "\n"
				+ "numberN bit length=" + numberN.bitLength() + "\n"
				+ "eulerFunction=" + eulerFunction + "\n" 
				+ "numberE=" + numberE + "\n"
				+ "numberD=" + numberD + "\n"
				+ "]";
	}
}
