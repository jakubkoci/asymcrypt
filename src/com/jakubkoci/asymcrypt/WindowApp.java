package com.jakubkoci.asymcrypt;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingWorker;

public class WindowApp extends JFrame {

	private static final int WINDOW_HEIGHT = 600;
	private static final int WINDOW_WIDTH = 500;

	public static void main(String[] args) {
		new WindowApp();
	}

	private JButton generateButton;
	private JButton encryptButton;
	private JButton decryptButton;
	private JTextArea originalMsgText;
	private JTextArea encryptedMsgText;
	private JScrollPane originalMsgScrollPane;
	private JScrollPane encryptedMsgScrollPane;
	private JLabel originalMsgLabel;
	private JLabel encryptedMsgLabel;
	private JTextField publicKeyText;
	private JTextField privateKeyText;
	private JLabel publicKeyLabel;
	private JLabel privateKeyLabel;
	private JTextField publicKeyFileText;
	private JLabel publicKeyFileLabel;
	private JTextField privateKeyFileText;
	private JLabel privateKeyFileLabel;
	private JLabel generateInfoLabel;

	public WindowApp() throws HeadlessException {
		super();
		initComponents();
		
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setLocationRelativeTo(null);
		setResizable(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("RSA encryption");
		setVisible(true);
	}

	private void initComponents() {		
		ButtonListener buttonListener = new ButtonListener();
		
		publicKeyText = new JTextField();		
		publicKeyLabel = new JLabel("Public key filename: ");
		publicKeyLabel.setLabelFor(publicKeyText);
		privateKeyText = new JTextField();
		privateKeyLabel = new JLabel("Private key filename: ");
		privateKeyLabel.setLabelFor(privateKeyText);
		generateButton = new JButton("Generate keys");
		generateButton.addActionListener(buttonListener);
		generateInfoLabel = new JLabel("");
		
		originalMsgLabel = new JLabel("Original message: ");
		originalMsgText = new JTextArea();
		originalMsgText.setLineWrap(true);
		originalMsgText.setEditable(true);
		
		originalMsgScrollPane = new JScrollPane(originalMsgText);
		originalMsgScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		originalMsgScrollPane.setVisible(true);
		
		publicKeyFileText = new JTextField();
		publicKeyFileText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				publicKeyFileText.setText(returnChoosenFile());
			}
		});
		publicKeyFileLabel = new JLabel("Public key file: ");
		publicKeyFileLabel.setLabelFor(publicKeyFileText);
		encryptButton = new JButton("Encrypt message");
		encryptButton.addActionListener(buttonListener);
		
		encryptedMsgLabel = new JLabel("Encrypted message: ");
		encryptedMsgText = new JTextArea();
		encryptedMsgText.setLineWrap(true);
		encryptedMsgText.setEditable(true);
		
		encryptedMsgScrollPane = new JScrollPane(encryptedMsgText);
		encryptedMsgScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		encryptedMsgScrollPane.setVisible(true);
		
		privateKeyFileText = new JTextField();		
		privateKeyFileText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				privateKeyFileText.setText(returnChoosenFile());
			}
		});
		privateKeyFileLabel = new JLabel("Private key file: ");
		privateKeyFileLabel.setLabelFor(privateKeyFileText);
		decryptButton = new JButton("Decrypt message");
		decryptButton.addActionListener(buttonListener);
		
		JPanel panel = new JPanel();
		GroupLayout layout = createLayout(panel);
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.setLayout(layout);

		add(panel);
		pack();
	}

	private GroupLayout createLayout(JPanel panel) {
		GroupLayout layout = new GroupLayout(panel);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addComponent(publicKeyLabel)
						.addComponent(publicKeyText))
			    .addGroup(layout.createSequentialGroup()
			    		.addComponent(privateKeyLabel)
			    		.addComponent(privateKeyText))
			    .addComponent(generateButton)
			    .addComponent(generateInfoLabel)
			    .addComponent(originalMsgLabel)
			    .addComponent(originalMsgScrollPane)
			    .addGroup(layout.createSequentialGroup()
			    		.addComponent(publicKeyFileLabel)
			    		.addComponent(publicKeyFileText)
			    		.addComponent(encryptButton))
			    .addComponent(encryptedMsgLabel)
			    .addComponent(encryptedMsgScrollPane)
			    .addGroup(layout.createSequentialGroup()
			    		.addComponent(privateKeyFileLabel)
			    		.addComponent(privateKeyFileText)
			    		.addComponent(decryptButton)));

		layout.setVerticalGroup(layout.createSequentialGroup()
			    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
			    		.addComponent(publicKeyLabel)
			    		.addComponent(publicKeyText))
			    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
			    		.addComponent(privateKeyLabel)
			    		.addComponent(privateKeyText))
			    .addComponent(generateButton)
			    .addComponent(generateInfoLabel)
			    .addComponent(originalMsgLabel)
			    .addComponent(originalMsgScrollPane)
			    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
			    		.addComponent(publicKeyFileLabel)
			    		.addComponent(publicKeyFileText)
			    		.addComponent(encryptButton))
			    .addComponent(encryptedMsgLabel)
			    .addComponent(encryptedMsgScrollPane)
			    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
			    		.addComponent(privateKeyFileLabel)
			    		.addComponent(privateKeyFileText)
			    		.addComponent(decryptButton)));
		return layout;
	}
	
	private String returnChoosenFile() {
		String currentDir = System.getProperty("user.dir");
		JFileChooser fileChooser = new JFileChooser(currentDir);
		int fileChooserResult = fileChooser.showOpenDialog(WindowApp.this);
		
		if (fileChooserResult == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			return file.getAbsolutePath();
		} else {
			return "";
		}
	}

	private void showErrorMessage(String message) {
		JOptionPane.showMessageDialog(WindowApp.this, message, 
				"ERROR", JOptionPane.ERROR_MESSAGE);
	}
	
	private void showInfoMessage(String message) {
		JOptionPane.showMessageDialog(WindowApp.this, message, 
				"INFO", JOptionPane.INFORMATION_MESSAGE);
	}

	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource().equals(generateButton)) {
				if (!publicKeyText.getText().isEmpty() && !privateKeyText.getText().isEmpty()) {
					try {
						final ProgressDialog progress = new ProgressDialog(WindowApp.this);
						progress.pack(); 
						progress.setLocationRelativeTo(null); 
						progress.setVisible(true); 
						
						class MyWorker extends SwingWorker<String, Void> {
							private static final int INPUT_BIT_LENGTH = 12;

							protected String doInBackground() {
								Key key = Key.generateRandom(INPUT_BIT_LENGTH);
								App.exportKeys(key, publicKeyText.getText(), privateKeyText.getText());								
								String currentDir = System.getProperty("user.dir");
								String message = "<html>"
										+ "Keys with length "
										+ key.getPublicKey().getNumberN().bitLength() 
										+ " b were generated generated into: " + "<br> \"" + currentDir + "\""
										+ "</html>";
								
								showInfoMessage(message);
						        return "Done.";
							}

							protected void done() {
								progress.setVisible(false);
							}
						}

						new MyWorker().execute();
						
					} catch (IllegalArgumentException ex) {
						showErrorMessage(ex.getMessage());
					}
				} else {
					showErrorMessage("Fill in file name for public and private key!");
				}
			}
			
			if (e.getSource().equals(encryptButton)) {
				if (!originalMsgText.getText().isEmpty() && !publicKeyFileText.getText().isEmpty()) {
					try {
						String encryptedMsg = App.encryptMessage(originalMsgText.getText(), publicKeyFileText.getText());
						encryptedMsgText.setText(encryptedMsg);	
					} catch (IllegalArgumentException ex) {
						showErrorMessage(ex.getMessage());
					} 
				} else {
					showErrorMessage("Fill in original message and file with public key!");
				}
			}
			
			if (e.getSource().equals(decryptButton)) {
				if (!encryptedMsgText.getText().isEmpty() && !privateKeyFileText.getText().isEmpty()) {
					try {
						String decryptedMsg = App.decryptMessage(encryptedMsgText.getText(), privateKeyFileText.getText());
						originalMsgText.setText(decryptedMsg);	
					} catch (NumberFormatException ex) {
						showErrorMessage("Unrecognizable encrypted message format! "
								+ "Fill in encrypted message by this application.");
					} catch (IllegalArgumentException ex) {
						showErrorMessage(ex.getMessage());
					}
				} else {
					showErrorMessage("Fill in encrypted message and file with private key!");
				}
			}
			
		}

	}
}
