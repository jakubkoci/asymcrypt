package com.jakubkoci.asymcrypt;

import java.awt.BorderLayout;
import java.awt.Frame;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class ProgressDialog extends JDialog {
	private JLabel label;
	private JProgressBar progressBar;

	public ProgressDialog(Frame owner) {
	    super(owner);
	    init();
    }

	private void init() {
		label = new JLabel();
		label.setText("Generating...");
		
		progressBar = new JProgressBar(); 
		progressBar.setIndeterminate(true);
		
		JPanel content = (JPanel) getContentPane(); 
        content.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        content.add(label, BorderLayout.NORTH);
        content.add(progressBar); 
 
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); 
    }
}
