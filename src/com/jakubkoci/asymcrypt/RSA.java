package com.jakubkoci.asymcrypt;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class RSA {
	
	private static final String DELIMITER = ";";
	
	public String encryptText(String text, PublicKey publicKey) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < text.length(); i++) {
			char charFromText = text.charAt(i);
			long code = encryptCode(encodeCharToCode(charFromText), publicKey);
			if (i > 0) {
				result.append(DELIMITER);
			}
			result.append(code);
		}	
		return result.toString();
	}

	public String decryptText(String encryptedText, PrivateKey privateKey) {
		StringBuilder result = new StringBuilder();
		for (String encryptedSequence : encryptedText.split(";")) {
			long encryptedCode = getCodeFromSequence(encryptedSequence);
			long decryptedCode = decryptCode(encryptedCode, privateKey);			
			result.append(decodeCodeToChar(decryptedCode));
		}
		return result.toString();
	}
	
	public long encryptCode(long code, PublicKey publicKey) {
		BigInteger m = BigInteger.valueOf(code);
		BigInteger e = publicKey.getNumberE();
		BigInteger n = publicKey.getNumberN();
		BigInteger c = m.modPow(e, n);

		return c.longValue();
	}

	public long decryptCode(long encryptedCode, PrivateKey privateKey) {
		BigInteger c = BigInteger.valueOf(encryptedCode);
		BigInteger d = privateKey.getNumberD();
		BigInteger n = privateKey.getNumberN();
		BigInteger m = c.modPow(d, n); 

		return m.longValue();
	}
	
	private long encodeCharToCode(char messageChar) {
		return (int) messageChar;
	}
	
	private char decodeCodeToChar(long code) {
		return (char) code;
	}

	private long getCodeFromSequence(String encryptedSequence) {
		return Long.valueOf(encryptedSequence);
	}
}
