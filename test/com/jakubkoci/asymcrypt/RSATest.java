package com.jakubkoci.asymcrypt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class RSATest {

	private static Key KEY_16;
	private PublicKey publicKey;
	private PrivateKey privateKey;
	private RSA rsa;
	private int code;

	@BeforeClass
	public static void createKeys() {
		KEY_16 = Key.generateRandom(16);
//		KEY_16 = Key.generateFromNumbers(977, 997);
		System.out.println("16 " + KEY_16);
	}
	
	@Before
	public void setUp() {
		publicKey = KEY_16.getPublicKey();
		privateKey = KEY_16.getPrivateKey();
		rsa = new RSA();
		code = 10;
	}
	
	@Test
	public void encryptedIsDifferentFromOriginal() {
		long encryptedMessage = rsa.encryptCode(code, publicKey);
		assertTrue(code != encryptedMessage);
	}
	
	@Test
	public void decryptedIsSameAsOriginal() {
		long encryptedMessage = rsa.encryptCode(code, publicKey);
		long decryptedMessage = rsa.decryptCode(encryptedMessage, privateKey);
		
		assertTrue(code == decryptedMessage);
	}
	
	@Test
	public void encrytedTextIsDiferentFromOriginal() {
		String text = "AHOJ";
		String encryptedText = rsa.encryptText(text, publicKey); 
		assertNotEquals(text, encryptedText);
	}
	
	@Test
	public void decryptedTextIsSameAsOriginal() {
		String original = "AHOJ";
		String encrypted = rsa.encryptText(original, publicKey); 
		String decrypted = rsa.decryptText(encrypted, privateKey);
		
		log(publicKey.toString());
		log(privateKey.toString());
		log("Original text: " + original);
		log("Encrypted text: " + encrypted);
		log("Decrypted text: " + decrypted);
		
		assertEquals(original, decrypted);
	}
	
	private static void log(String s) {
		System.out.println(s);
	}
}
