package com.jakubkoci.asymcrypt;

import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class KeyTest {
	
	private static Key KEY_16;
	
	@BeforeClass
	public static void createKeys() {
		KEY_16 = Key.generateRandom(12);
		System.out.println("16 " + KEY_16);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void creationWithoutPrimeNumberShloudFailed() {
		Key.generateFromNumbers(397, 400);
	}

	@Test(expected = IllegalArgumentException.class)
	public void creationWithTooLowPrimeNumberShloudFailed() {
		Key.generateFromNumbers(97, 107);
	}
	
	@Test
	public void randomGeneratedKeyIsNotNull() {
		Key key = Key.generateRandom();
		System.out.println(key);
		assertTrue(key != null);
	}
	
	@Test
	public void numberNFrom16BitHasEnoughBitLength() {
		int bitLength = KEY_16.getPublicKey().getNumberN().bitLength(); 
		assertTrue(bitLength >= 32);
	}
	
	@Test
	public void numberEFrom16IsPositive() {
		assertTrue(KEY_16.getPublicKey().getNumberE().compareTo(BigInteger.ZERO) > 0);
	}
	
	@Test
	public void numberDFrom16IsPositive() {
		assertTrue(KEY_16.getPrivateKey().getNumberD().compareTo(BigInteger.ZERO) > 0);
	}
	
	@Test
	public void numberNFrom16IsPositive() {
		assertTrue(KEY_16.getPublicKey().getNumberN().compareTo(BigInteger.ZERO) > 0);
	}
	
	@Test
	public void publicNumberNFrom16BitIsEuqualToPrivate() {
		assertTrue(KEY_16.getPublicKey().getNumberN().equals(KEY_16.getPrivateKey().getNumberN()));
	}
}
