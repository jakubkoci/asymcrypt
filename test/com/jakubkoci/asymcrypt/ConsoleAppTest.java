package com.jakubkoci.asymcrypt;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Ignore;
import org.junit.Test;

public class ConsoleAppTest {

	@Test
	@Ignore
	public void keyFilesAreGenerated() {
		String[] params = new String[] {"gen", "public", "private"};
		ConsoleApp.main(params);
		File publicKeyFile = new File("public");
		File privateKeyFile = new File("private");
		assertTrue(publicKeyFile.exists());
		assertTrue(privateKeyFile.exists());
		publicKeyFile.delete();
		privateKeyFile.delete();
	}
	
	@Test
	@Ignore
	public void returnMessageEncryptedByKey() {
		String[] params = new String[] {"encrypt", "public", "AHOJ"};
		ConsoleApp.main(params);
	}
	
	@Test
	@Ignore
	public void returnMessageDecryptedByKey() {
		String[] params = new String[] {"decrypt", "private", "00026375000109170002651300018327"};
		ConsoleApp.main(params);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void runWithoutParameters() {
		String[] params = new String[0];
		ConsoleApp.main(params);
	}
}
